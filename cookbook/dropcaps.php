<?php if (!defined('PmWiki')) exit();

if (version_compare(phpversion(), '5.5', '<')) {
  echo "Dropcaps will not work - please update your php-version to 5.5*";
}

$RecipeInfo['dropcaps']['Version']='2017-02-24';
$HTMLHeaderFmt[] = "<link rel='stylesheet' href='\$PubDirUrl/css/dropcaps.css' type='text/css' />";
Markup_e('dropcaps',
  'fulltext',
  "/\\(:big:\\)(.*)\\(:big:\\)/si",
  "CallbackDropcaps"
);

function CallbackDropcaps($m) {
  return "<span class=\"dropcaps\">" . $m[1] . "</span>";
}

