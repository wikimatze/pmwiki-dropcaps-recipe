dropcaps makes the first letter in a Wiki text big.

![Picture of the recipe](http://www.pmwiki.org/pmwiki/uploads/Cookbook/dropcaps.png)


## Questions Answered By This Recipe

How can I emphasize the first letter of a text?


## Description

This recipe will add a special syntax which through you can make letter or even whole words looking more appealing.


## Installation

- download from [source][source] or via git `git clone
https://bitbucket.org/wikimatze/pmwiki-dropcaps-recipe.git `
- make the following copy-tasks:
  - put `dropcaps.php` in your cookbook directory (`cp pmwiki-dropcaps-recipe/cookbook/dropcaps.php cookbook/`)
  - put `dropcaps.css` in your public CSS directory (normally *pub/css*) (`cp pmwiki-dropcaps-recipe/css/dropcaps.css pub/css/`)
- add `include_once("$FarmD/cookbook/dropcaps.php");` in your *config.php*.


## Usage

Use the following syntax `(:big:)*(:big:)`, where you can replace \* with a letter, a word or a phrase.


## Notes

I got the idea from [Smashingmagazine CSS Tricks][smashing].


## Comments/Feedback

See [Dropcaps-Talk][talk] - your comments are welcome here!


## Contact

Feature request, bugs, questions, and so on can be send to <matthias@wikimatze.de>. If you enjoy the script
please leave your comment under [Dropcaps Users][Dropcaps Users].


## License ##

This software is licensed under the [MIT license][mit].

© 2011-2017 Matthias Guenther <matthias@wikimatze.de>.

[Dropcaps Users]: http://www.pmwiki.org/wiki/Cookbook/Dropcaps-Users
[mit]: http://en.wikipedia.org/wiki/MIT_License
[smashing]: http://www.smashingmagazine.com/2009/11/23/6-useful-coding-solutions-for-designers-and-developers/
[source]: https://bitbucket.org/wikimatze/pmwiki-dropcaps-recipe/overview
[talk]: http://www.pmwiki.org/wiki/Cookbook/Dropcaps-Talk

/* vim: set ts=2 sw=2 textwidth=120: */
